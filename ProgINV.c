#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>



char *strrev(char *str)
{
    if (!str || ! *str)
        return str;

    int i = strlen(str)-1;
    int j = 0;

    char ch;
    while (i > j)
    {
        ch = str[i];
        str[i] = str[j];
        str[j] = ch;
        i--;
        j++;
    }
    return str;
}

void *inv(void *v)
{
    char *s1 =  (char *) v;
  
    printf("\n%s\n",s1);

    strrev(s1);

  //  printf("%s",s1);
  //
    pthread_exit((void*)s1);
}

int main(int argc, char *argv[])
{
  char *s;
  char *rez;
  s=argv[1];

  pthread_t thr;
  if(pthread_create(&thr,NULL,inv,s))
  {
        perror(NULL);
        return errno;

  }
  if (pthread_join(thr, (void**)&rez)) {
        perror(NULL);
        return errno;
  }
  printf("%s\n\n ",rez);

  
  return 0;}
